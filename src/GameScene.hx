
import world.World;

import common.Assets;
import common.ecs.System;
import common.ecs.Entity;
import common.ecs.Component;

import world.Factory;
import world.components.SurgeComponent;
import world.messages.BreakEntityMessage;


class GameScene extends common.Scene {

    var scene: h2d.Scene;

    var world: World;
    var assets: common.Assets;

    public function new(scene: h2d.Scene, assets: common.Assets) {
        this.scene = scene;
        this.assets = assets;
        Factory.initFactory(assets);

        this.world = new World(assets, scene);
        this.world.restartGame();
    }

    override public function update(dt: Float) {
        this.world.update(dt);
    }

    override public function render(engine: h3d.Engine) {}

    override public function onEvent(event: hxd.Event) {
        if (this.world != null) { this.world.onEvent(event); }
    }
}
