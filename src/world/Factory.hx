
package world;

import hxd.Rand;

import common.ecs.Entity;

import world.Constants;
import world.WorldTile;
import world.components.GridComponent;
import world.components.RenderComponent;
import world.components.SpaceComponent;
import world.components.PlayerComponent;
import world.components.LightComponent;
import world.components.BreakableComponent;
import world.components.ItemComponent;
import world.components.OxygenComponent;
import world.components.SurgeComponent;
import world.components.PowerComponent;
import world.components.ControlComponent;
import world.components.TutorialComponent;

class Factory {

    static var _instance: Factory = null;

    public static function get() {
        return _instance;
    }

    var assets: common.Assets;
    var rand: hxd.Rand;

    function new(assets) {
        this.assets = assets;
        this.rand = new Rand(Random.int(0, 100000));
    }

    public static function initFactory(assets: common.Assets): Factory {
        _instance = new Factory(assets);
        return _instance;
    }

    public function makeTile(r: Null<Int> = null): WorldTile {
        var tileAssets = this.assets.getAsset("tiles");
        if (r == null) {
            r = this.rand.random(100000);
        }
        var ind = r % tileAssets.count;
        var t = new WorldTile(tileAssets.getBitmap(ind));
        return t;
    }

    public function makeWall(x: Int = 0, y: Int = 0): Entity {
        var e = Entity.newEntity();
        var c = this.assets.getAsset("wall").getBitmap();
        e.add(c, 0);

        e.addComponent(new GridComponent(x, y));
        e.x = x * Constants.GridSize;
        e.y = y * Constants.GridSize;

        var s = new SpaceComponent();
        s.occupySpace = true;
        s.blockVision = true;
        e.addComponent(s);

        return e;
    }

    public function makePlayer(x: Int = 0, y: Int = 0): Entity {
        var e = Entity.newEntity();
        var draws = new Map<String, h2d.Object>();
        draws["default"] = this.assets.getAsset("player").getBitmap();
        draws["fog"] = draws["default"];
        var rc = new RenderComponent(draws);
        rc.index = 1;
        e.addComponent(rc);

        e.addComponent(new GridComponent(x, y));
        e.x = x * Constants.GridSize;
        e.y = y * Constants.GridSize;

        var s = new SpaceComponent();
        s.occupySpace = true;
        e.addComponent(s);

        e.addComponent(new PlayerComponent());
        e.addComponent(new LightComponent());

        return e;
    }

    public function makeWire(x: Int, y: Int = 0, direction: Int, isBroken: Bool=false): Entity {
        var e = Entity.newEntity();
        var draws = new Map<String, h2d.Object>();
        var broken = new h2d.Layers();
        if (direction == 0) { // horizontal
            draws["default"] = this.assets.getAsset("wire").getBitmap(0);
            draws["fog"] = draws["default"];
            broken.add(this.assets.getAsset("wire").getBitmap(1), 0);
        } else { // vertical
            draws["default"] = this.assets.getAsset("wire").getBitmap(2);
            draws["fog"] = draws["default"];
            broken.add(this.assets.getAsset("wire").getBitmap(3), 0);
        }
        var anim = this.assets.getAsset("spark").getAnim(2, function(_, _) { return this.rand.random(10) - 5; });
        broken.add(anim, 1);
        draws["broken"] = broken;

        e.addComponent(new RenderComponent(draws, "fog"));

        e.addComponent(new GridComponent(x, y));
        e.x = x * Constants.GridSize;
        e.y = y * Constants.GridSize;

        e.addComponent(new BreakableComponent(isBroken));
        e.addComponent(new SurgeComponent());

        var s = new SpaceComponent();
        s.occupySpace = false;
        e.addComponent(s);

        e.addComponent(new TutorialComponent("wire"));

        return e;
    }

    public function makeFire(x: Int, y: Int = 0): Entity {
        var e = Entity.newEntity();

        var anim = this.assets.getAsset("fire").getAnim(2, function(_, _) { return this.rand.random(10) - 5; });

        var draws = new Map<String, h2d.Object>();
        draws["default"] = anim;
        draws["broken"] = draws["default"];

        e.addComponent(new RenderComponent(draws, "fog"));

        e.addComponent(new GridComponent(x, y));
        e.x = x * Constants.GridSize;
        e.y = y * Constants.GridSize;

        var s = new SpaceComponent();
        s.occupySpace = true;
        e.addComponent(s);

        e.addComponent(new OxygenComponent(Consume, 250));
        e.addComponent(new BreakableComponent(true, true, "extinguisher"));
        e.addComponent(new TutorialComponent("fire"));

        return e;
    }

    public function makeControl(x: Int, y: Int = 0, position: Int): Entity {
        var e = Entity.newEntity();

        var draws = new Map<String, h2d.Object>();
        draws["default"] = this.assets.getAsset("control").getBitmap(position);
        draws["fog"] = draws["default"];

        e.addComponent(new RenderComponent(draws, "fog"));

        e.addComponent(new GridComponent(x, y));
        e.x = x * Constants.GridSize;
        e.y = y * Constants.GridSize;

        var s = new SpaceComponent();
        s.occupySpace = true;
        e.addComponent(s);

        e.addComponent(new ControlComponent());

        return e;
    }

    public function makeVent(x: Int, y: Int = 0, isBroken: Bool = false): Entity {
        var e = Entity.newEntity();

        var draws = new Map<String, h2d.Object>();
        draws["default"] = this.assets.getAsset("vent").getBitmap(0);
        draws["fog"] = draws["default"];
        draws["broken"] = this.assets.getAsset("vent").getBitmap(1);

        e.addComponent(new RenderComponent(draws, "fog"));

        e.addComponent(new GridComponent(x, y));
        e.x = x * Constants.GridSize;
        e.y = y * Constants.GridSize;

        e.addComponent(new BreakableComponent(isBroken));
        e.addComponent(new OxygenComponent(Fluctuating, 1000));

        var s = new SpaceComponent();
        s.occupySpace = true;
        s.blockVision = true;
        e.addComponent(s);

        e.addComponent(new TutorialComponent("vent"));

        return e;
    }

    public function makePowerStation(x: Int, y: Int = 0): Entity {
        var e = Entity.newEntity();

        var draws = new Map<String, h2d.Object>();
        draws["default"] = this.assets.getAsset("powerstation").getBitmap(0);
        draws["fog"] = draws["default"];

        e.addComponent(new RenderComponent(draws, "fog"));

        e.addComponent(new GridComponent(x, y));
        e.x = x * Constants.GridSize;
        e.y = y * Constants.GridSize;

        var s = new SpaceComponent();
        s.occupySpace = true;
        s.blockVision = true;
        e.addComponent(s);

        e.addComponent(new PowerComponent());

        e.addComponent(new TutorialComponent("station"));
        var cc = new ControlComponent();
        cc.showAllPower = false;
        cc.showAllOxygen = false;
        cc.showThisPower = true;
        e.addComponent(cc);

        return e;
    }

    public function makeTank(x: Int, y: Int = 0): Entity {
        var e = Entity.newEntity();

        var draws = new Map<String, h2d.Object>();
        draws["default"] = this.assets.getAsset("tank").getBitmap(0);
        draws["fog"] = draws["default"];

        var broken = new h2d.Layers();
        var anim = this.assets.getAsset("oxygen").getAnim(2, function(_, _) { return this.rand.random(10) - 5; });
        broken.add(this.assets.getAsset("tank").getBitmap(1), 0);
        broken.add(anim, 1);
        draws["broken"] = broken;

        e.addComponent(new RenderComponent(draws, "fog"));

        e.addComponent(new GridComponent(x, y));
        e.x = x * Constants.GridSize;
        e.y = y * Constants.GridSize;

        e.addComponent(new BreakableComponent(false));
        e.addComponent(new OxygenComponent(Leak, 300));

        var s = new SpaceComponent();
        s.occupySpace = true;
        s.blockVision = true;
        e.addComponent(s);

        e.addComponent(new TutorialComponent("tank"));

        return e;
    }

    public function makeCell(x: Int, y: Int = 0): Entity {
        var e = Entity.newEntity();

        var draws = new Map<String, h2d.Object>();
        draws["default"] = this.assets.getAsset("cell").getBitmap(0);

        e.addComponent(new RenderComponent(draws, "fog"));

        e.addComponent(new GridComponent(x, y));
        e.x = x * Constants.GridSize;
        e.y = y * Constants.GridSize;

        var s = new SpaceComponent();
        s.occupySpace = false;
        e.addComponent(s);

        e.addComponent(new ItemComponent("cell"));
        e.addComponent(new TutorialComponent("cell"));

        return e;
    }

    public function makeExtinguisher(x: Int, y: Int = 0): Entity {
        var e = Entity.newEntity();

        var draws = new Map<String, h2d.Object>();
        draws["default"] = this.assets.getAsset("extinguisher").getBitmap(0);

        e.addComponent(new RenderComponent(draws, "fog"));

        e.addComponent(new GridComponent(x, y));
        e.x = x * Constants.GridSize;
        e.y = y * Constants.GridSize;

        var s = new SpaceComponent();
        s.occupySpace = false;
        e.addComponent(s);

        e.addComponent(new ItemComponent("extinguisher"));
        e.addComponent(new TutorialComponent("extinguisher"));

        return e;
    }

}
