
package world;

import hxd.Rand;

import common.Assets;
import common.Mailbox;
import common.ChunkMap;
import common.Point2i;
import common.ecs.Entity;

import world.WorldTile;

import world.components.GridComponent;

import world.messages.MapSetMessage;
import world.messages.RecalculateFogMessage;
import world.messages.GameRestartMessage;

import world.systems.RenderSystem;
import world.systems.MovementSystem;
import world.systems.InputSystem;
import world.systems.HUDRenderSystem;
import world.systems.InventorySystem;
import world.systems.BrokenSystem;
import world.systems.OxygenSystem;
import world.systems.PowerSystem;
import world.systems.RepairSystem;
import world.systems.TutorialSystem;
import world.systems.IncidentSystem;

class World extends common.ecs.World {

    public var map: common.ChunkMap<WorldTile>;

    var rand: hxd.Rand;
    var scene: h2d.Scene;

    var defaultPlayerPosition: Point2i;

    var oxygenSystem: OxygenSystem;
    public var oxygenLevel(get, null): Int;

    var powerSystem: PowerSystem;
    public var power(get, null): Float;
    public var powerConsumption(get, null): Float; // per sec

    public function get_oxygenLevel(): Int {
        return Std.int(this.oxygenSystem.oxygenLevel / 100);
    }

    public function get_power(): Float {
        return this.powerSystem != null ? this.powerSystem.powerLevel : 0.0;
    }

    public function get_powerConsumption(): Float {
        return this.powerSystem != null ? this.powerSystem.powerConsumption : 0.0;
    }

    public function new(assets: Assets, scene: h2d.Scene) {
        super();
        this.scene = scene;
        this.rand = new hxd.Rand(Random.int(0, 100000));

        this.addSystem(new RenderSystem(scene));
        this.addSystem(new IncidentSystem(scene));
        this.oxygenSystem = new OxygenSystem();
        this.addSystem(this.oxygenSystem);
        this.powerSystem = new PowerSystem();
        this.addSystem(this.powerSystem);
        this.addSystem(new EntityMovementSystem());
        this.addSystem(new InputSystem());
        this.addSystem(new HUDRenderSystem(assets, scene));
        this.addSystem(new InventorySystem(scene));
        this.addSystem(new BrokenSystem());
        this.addSystem(new RepairSystem());
        this.addSystem(new TutorialSystem());

        this.newMap();
    }

    public function restartGame() {
        Mailbox.get("world").dispatch(new GameRestartMessage());
    }

    public function newMap() {
        this.map = new ChunkMap<WorldTile>(64);
        this.generateFloor();
        Mailbox.get("world").dispatch(new MapSetMessage(this.map));
    }

    override public function addEntity(e: Entity, addToSystems=true) {
        super.addEntity(e, addToSystems);
        // TODO: this should be in a map system, probably
        var gridComp = cast(e.getComponent(GridComponent.TYPE), GridComponent);
        if (gridComp != null) {
            this.map.get(gridComp.x, gridComp.y).addEntity(e);
            e.x = gridComp.x * Constants.GridSize;
            e.y = gridComp.y * Constants.GridSize;
        }
    }
    override public function onEntityRemoved(entity: Entity) {
        var gc = cast(entity.getComponent(GridComponent.TYPE), GridComponent);
        if (gc != null) {
            var tile = this.map.get(gc.x, gc.y);
            tile.removeEntity(entity);
        }
    }

    public function generateFloor() {
        var factory = Factory.get();
        for (x in 0...Constants.WorldWidth) {
            for (y in 0...Constants.WorldHeight) {
                var t = factory.makeTile();
                t.x = Constants.GridSize * x;
                t.y = Constants.GridSize * y;
                this.setTile(t, x, y);
            }
        }

        var player: Entity = null;

        for (y in 0...Constants.WorldHeight) {
            for (x in 0...Constants.WorldWidth) {
                var char = Constants.WorldLayout[y].charAt(x);
                var e: Entity = null;
                if (char == "#") {
                    e = factory.makeWall(x, y);
                } else if (char == "@") {
                    if (player != null) {
                        trace("Only one player can be added");
                        continue;
                    }
                    e = factory.makePlayer(x, y);
                    player = e;
                    this.defaultPlayerPosition = [x, y];
                } else if (char == "|" || char == "-") {
                    e = factory.makeWire(x, y, char == "-" ? 0 : 1);
                } else if (char == "]" || char == "-") {
                    e = factory.makeWire(x, y, char == "-" ? 0 : 1, true);
                } else if (char == "f") {
                    e = factory.makeFire(x, y);
                } else if (char == "0") {
                    e = factory.makeControl(x, y, 0);
                } else if (char == "1") {
                    e = factory.makeControl(x, y, 1);
                } else if (char == "2") {
                    e = factory.makeControl(x, y, 2);
                } else if (char == "x") {
                    e = factory.makeExtinguisher(x, y);
                } else if (char == "c") {
                    e = factory.makeCell(x, y);
                } else if (char == "v") {
                    e = factory.makeVent(x, y);
                } else if (char == "W") {
                    e = factory.makeVent(x, y, true);
                } else if (char == "P") {
                    e = factory.makePowerStation(x, y);
                } else if (char == "O") {
                    e = factory.makeTank(x, y);
                }

                if (e != null && e != player) {
                    this.addEntity(e);
                }
            }
        }

        this.addEntity(player);
    }

    public function setTile(tile: WorldTile, x: Int, y: Int) {
        var old = this.map.get(x, y);
        this.map.set(x, y, tile);
    }
}
