
package world.messages;

import world.WorldTile;
import common.ChunkMap;
import common.Message;

class MapSetMessage extends Message {

    public static final TYPE = "MapSetMessage";
    override public function get_type(): String { return TYPE; }

    public var map(default, null): ChunkMap<WorldTile>;

    public function new(map: ChunkMap<WorldTile>) {
        super();
        this.map = map;
    }
}
