
package world.messages;

import common.Message;

class RecalculateFogMessage extends Message {
    public static final TYPE = "RecalculateFogMessage";
    override public function get_type(): String { return TYPE; }
}
