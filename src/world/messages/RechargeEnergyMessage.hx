
package world.messages;

import common.Message;
import common.ecs.Entity;

class RechargeEnergyMessage extends Message {
    public static final TYPE = "RechargeEnergyMessage";
    override public function get_type(): String { return TYPE; }

    public var entity: Entity;

    public function new(entity: Entity) {
        super();
        this.entity = entity;
    }
}
