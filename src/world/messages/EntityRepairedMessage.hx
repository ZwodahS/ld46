
package world.messages;

import common.Message;
import common.ecs.Entity;

class EntityRepairedMessage extends Message {
    public static final TYPE = "EntityRepairedMessage";
    override public function get_type(): String { return TYPE; }

    public var entity: Entity;

    public function new(entity: Entity) {
        super();
        this.entity = entity;
    }

}
