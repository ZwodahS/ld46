
package world.messages;

import common.Message;
import common.ecs.Entity;

class RepairEntityMessage extends Message {
    public static final TYPE = "RepairEntityMessage";
    override public function get_type(): String { return TYPE; }

    public var entity: Entity;

    public function new(entity: Entity) {
        super();
        this.entity = entity;
    }
}
