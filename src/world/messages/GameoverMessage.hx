
package world.messages;

import common.Message;

enum Reason {
    LowOxygenLevel;
    HighOxygenLevel;
    NoPower;
}

class GameoverMessage extends Message {
    public static final TYPE = "GameoverMessage";
    override public function get_type(): String { return TYPE; }

    public var reason: Reason;

    public function new(reason: Reason) {
        super();
        this.reason = reason;
    }
}
