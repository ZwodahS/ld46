
package world.messages;

import common.Message;

class IncidentRateIncreaseMessage extends Message {
    public static final TYPE = "IncidentRateIncreaseMessage";
    override public function get_type(): String { return TYPE; }

    public var amount: Float;

    public function new(amount: Float) {
        super();
        this.amount = amount;
    }
}
