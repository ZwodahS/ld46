
package world.messages;

import common.Message;

import common.ecs.Entity;

class PickupRequestMessage extends Message {
    public static final TYPE = "PickupRequestMessage";
    override public function get_type(): String { return TYPE; }

    public var entity(default, null): Entity;

    public function new(entity: Entity) {
        super();
        this.entity = entity;
    }
}
