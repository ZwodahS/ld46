
package world.messages;

import common.Message;
import common.ecs.Entity;

class EntityTouchedMessage extends Message {
    public static final TYPE = "EntityTouchedMessage";
    override public function get_type(): String { return TYPE; }

    public var entities: Array<Entity>;
    public function new(entities: Array<Entity>) {
        super();
        this.entities = entities;
    }
}
