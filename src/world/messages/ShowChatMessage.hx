
package world.messages;

import common.Message;

class ShowChatMessage extends Message {
    public static final TYPE = "ShowChatMessage";
    override public function get_type(): String { return TYPE; }

    public var text: String;
    public var location: String;

    public function new(text: String, location: String = "top") {
        super();
        this.text = text;
        this.location = location;
    }
}
