
package world.messages;

import common.Message;

import common.Point2i;
import common.ecs.Entity;

class EntityMovedMessage extends Message {
    public static final TYPE = "EntityMovedMessage";
    override public function get_type(): String { return TYPE; }

    public var entity(default, null): Entity;
    public var exiting(default, null): Point2i;
    public var entering(default, null): Point2i;

    public function new(entity: Entity, exiting: Point2i, entering: Point2i) {
        super();
        this.entity = entity;
        this.exiting = exiting;
        this.entering = entering;
    }
}
