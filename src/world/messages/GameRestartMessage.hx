
package world.messages;

import common.Message;

class GameRestartMessage extends Message {
    public static final TYPE = "GameRestartMessage";
    override public function get_type(): String { return TYPE; }

    public function new() {
        super();
    }
}
