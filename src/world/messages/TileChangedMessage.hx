
package world.messages;

import world.WorldTile;

import common.ecs.Entity;
import common.Message;

class TileChangedMessage extends Message {

    public static final TYPE = "TilesChangeMessage";
    override public function get_type(): String { return TYPE; }

    public var oldTile(default, null): WorldTile;
    public var newTile(default, null): WorldTile;

    public function new(oldTile: WorldTile, newTile: WorldTile) {
        super();
        this.oldTile = oldTile;
        this.newTile = newTile;
    }
}
