
package world.messages;

import common.Message;
import common.Point2i;
import common.ecs.Entity;

class EntityMoveRequestMessage extends Message {

    public static final TYPE = "EntityMoveRequestMessage";
    override public function get_type(): String { return TYPE; }

    public var entity(default, null): Entity;
    public var moveAmount(default, null): Point2i;

    public function new(entity: Entity, moveAmount: Point2i) {
        super();
        this.entity = entity;
        this.moveAmount = moveAmount;
    }
}
