
package world.messages;

import common.Message;

class HideChatMessage extends Message {
    public static final TYPE = "HideChatMessage";
    override public function get_type(): String { return TYPE; }

    public function new() {
        super();
    }
}
