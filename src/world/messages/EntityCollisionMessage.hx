
package world.messages;

import common.Message;
import common.Point2i;

import common.ecs.Entity;

class EntityCollisionMessage extends Message {

    public static final TYPE = "EntityCollisionMessage";
    override public function get_type(): String { return TYPE; }

    public var collider(default, null): Entity;
    public var collidee(default, null): Entity;

    public var colliderPosition(default, null): Point2i;
    public var collideePosition(default, null): Point2i;

    public function new(collider: Entity, collidee: Entity, colliderPosition: Point2i, collideePosition: Point2i) {
        super();
        this.collider = collider;
        this.collidee = collidee;
        this.colliderPosition = colliderPosition;
        this.collideePosition = collideePosition;
    }
}
