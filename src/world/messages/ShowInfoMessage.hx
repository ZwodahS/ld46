
package world.messages;

import common.Message;
import common.Point2i;

class ShowInfoMessage extends Message {
    public static final TYPE = "ShowInfoMessage";
    override public function get_type(): String { return TYPE; }

    public var position: Point2i;
    public var infoType: String;

    public function new(position: Point2i, infoType: String) {
        super();
        this.position = position;
        this.infoType = infoType;
    }
}
