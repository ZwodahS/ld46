
package world.messages;

import common.Message;

import common.ecs.Entity;

class BreakEntityMessage extends Message {
    public static final TYPE = "BreakEntityMessage";
    override public function get_type(): String { return TYPE; }

    public var entity: Entity;
    public function new(entity: Entity) {
        super();
        this.entity = entity;
    }
}
