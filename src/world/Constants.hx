
package world;

class Constants {
    public static final BaseOxygenLevel = 10000;
    public static final GridSize = 16;

    public static final WorldWidth = 30;
    public static final WorldHeight = 20;

    public static final WorldLayout = [
        "##############################",
        "#........##########O-O-#######",
        "#........#--------#....#######",
        "#........#........#O-O-#######",
        "#######..#........####.###v###",
        "#...........................|#",
        "###v###...##########...##..###",
        "#######...#...012--#---##.P###",
        "#######...#....@..x#...#######",
        "###v###...####ff####...##v####",
        "#|..........................|#",
        "#|.#####..####..#####..####.|#",
        "#|.####v..#.........#..v###.|#",
        "#|.#####..#.........#..####.|#",
        "#|.#####..#.........#..####.|#",
        "#|........####..#####.......|#",
        "##..###...####..#####...##..##",
        "##P.v##.................#v.P##",
        "#######-----------------######",
        "##############..##############"
    ];

    public static final FirstMessage = (
        "Hi, this is Earth Control. Mars has just been hit by the biggest sandstorm we have ever seen. The powerplant you are in is the last one left. Please keep it alive at all cost.\n" +
        "We have detected that there is fire in the station. Please pick up the fire extinguisher using [Space] and put out the fire\n" +
        "Go near the control panel to see the station information. Please keep the oxygen level around 100%. Too high or too low will kill you.\n" +
        "Please keep the station alive for 5 minutes, we are trying to get to you.\n\n\n" +
        "Press [Space] to close."
        );


    public static final VictoryMessage = (
        "Thanks for keeping the power plant alive for so long. We have downloaded all the important data.\n"+
        "Sorry for not telling you the truth earlier ... There is nothing we can do, and a bigger sandstorm is heading your way.\n\n"+
        "Good luck ...\n\n"+
        "We are sorry..."
    );

    public static final BrokenVentMessage = (
        "That is a clogged vent. You should clean that up or the oxygen level will fluctuates."
    );

    public static final BrokenTankMessage = (
        "That is a broken tank. That explains the increasing oxygen level."
    );

    public static final BrokenWireMessage = (
        "Oh a broken wire. You should fix that, or it will drain our energy even faster."
    );

    public static final PowerStationMessage = (
        "One of the 3 power station in the building. You should check and ensure that it still have power."
    );

    public static final PowerStationLowMessage = (
        "The energy level is low, you should find a power cell and replace it quickly."
    );

    public static final EnergyCell = (
        "Here they are... the energy cells for the power station"
    );

    public static final LoseLowOxygen = (
        "Hey are you okay ?? We see that the oxygen level is really low .....\n"+
        "You there ????\n\n"+
        "Okay guys .. no response ...\nLet's see how much we have.\n\n\n"+
        "Press [R] to restart"
    );

    public static final LoseHighOxygen = (
        "Hey are you okay ?? The oxygen is reallllly high ... you need to ... ohh.. you dead.. \n"+
        "Okay guys .. dead man .... Let's continue.\n\n\n"+
        "Press [R] to restart"
    );

    public static final LoseNoPower = (
        "................ No Power ........................\n\n\n"+
        "Press [R] to restart"
    );

    public static final VictoryTimer = 5 * 60;
    public static final IncidentRollRate = 2;
}
