
package world;

import common.ecs.Entity;

class WorldTile extends h2d.Layers {

    var background: h2d.Drawable;
    public var fogLayer: h2d.Drawable;
    public var entities(default, null): Map<Int, Entity>;
    public var fog(default, set): Bool;

    public function new(background: h2d.Drawable) {
        super();
        this.background = background;
        this.add(this.background, 0);
        this.entities = new Map<Int, Entity>();

        this.set_fog(true);
    }

    public function addEntity(entity: Entity): Entity {
        this.entities[entity.id] = entity;
        return entity;
    }

    public function removeEntity(entity: Entity): Entity {
        var entity = this.entities[entity.id];
        this.entities.remove(entity.id);
        return entity;
    }

    public function set_fog(fog: Bool): Bool {
        this.fog = fog;
        if (this.fogLayer != null) {
            this.fogLayer.visible = this.fog;
        }
        return this.fog;
    }
}

