
package world.systems;

import hxd.Rand;

import common.Mailbox;
import common.MathUtils;
import common.ChunkMap;
import common.Message;
import common.Point2i;
import common.ecs.System;
import common.ecs.Entity;

import world.WorldTile;
import world.components.BreakableComponent;
import world.messages.GameRestartMessage;
import world.messages.IncidentRateIncreaseMessage;
import world.messages.MapSetMessage;
import world.messages.BreakEntityMessage;

class IncidentSystem extends System {

    var map: ChunkMap<WorldTile>;

    var incidentRate: Float;

    var timePassed: Float = 0;
    var incidentTime: Float = 0;

    var rand: Rand;
    var entities: Array<Entity>;

    var scene: h2d.Scene;
    var flashLayer: h2d.Object;

    public function new(scene: h2d.Scene) {
        super();
        this.scene = scene;
        this.flashLayer = new h2d.Bitmap(h2d.Tile.fromColor(0xFF0000, 480, 320));
        this.flashLayer.alpha = 0.2;
        this.scene.add(flashLayer, 100);
        this.rand = new Rand(Random.int(0, 100000));
        this.entities = new Array<Entity>();

        var mailbox = Mailbox.get("world");
        mailbox.listen(GameRestartMessage.TYPE, function(m: Message) {
            this.timePassed = 0;
            this.incidentRate = 1.0;
        });

        mailbox.listen(MapSetMessage.TYPE, function(m: Message) {
            var message = cast(m, MapSetMessage);
            this.map = message.map;
        });

        mailbox.listen(IncidentRateIncreaseMessage.TYPE, function(m: Message) {
            var message = cast(m, IncidentRateIncreaseMessage);
            this.incidentRate += message.amount;
        });

    }

    override public function addEntity(entity: Entity) {
        super.addEntity(entity);
        var bc = cast(entity.getComponent(BreakableComponent.TYPE), BreakableComponent);
        if (bc != null) {
            this.entities.push(entity);
        }
    }

    override public function removeEntity(entity: Entity): Entity {
        this.entities.remove(entity);
        return entity;
    }

    override public function update(dt: Float) {
        if (this.flashLayer.alpha != 0) {
            this.flashLayer.alpha = MathUtils.clampF(
                    this.flashLayer.alpha - (dt * 0.2),
                    0.0,
                    1.0
            );
        }
        if (this.world.paused) { return; }
        this.timePassed += dt;
        this.incidentTime += dt;
        if (this.incidentTime > Constants.IncidentRollRate) {
            this.incidentTime -= Constants.IncidentRollRate;
            // roll 1 to 100, chance increase by 1% every 30 seconds
            // if incidentRate increase then it will higher chance of hitting
            var chance = (10 + (this.timePassed/30)) * this.incidentRate;
            if (this.rand.random(100) < chance) {
                this.flashLayer.alpha = 0.2;
                var r = this.rand.random(2);
                if (r > 0) {
                    var breakEntity = this.entities[this.rand.random(this.entities.length)];
                    Mailbox.get("world").dispatch(new BreakEntityMessage(breakEntity));
                } else {
                    var position: Point2i = [
                        this.rand.random(Constants.WorldWidth),
                        this.rand.random(Constants.WorldHeight),
                    ];
                    var tile = this.map.get(position.x, position.y);
                    var count = 0;
                    for (e in tile.entities) {
                        count += 1;
                    }
                    if (count == 0) {
                        var e: Entity = Factory.get().makeFire(position.x, position.y);
                        this.world.addEntity(e);
                    }
                }
            }
        }

    }
}
