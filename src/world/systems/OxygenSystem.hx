
package world.systems;

import hxd.Rand;

import common.ecs.System;
import common.ecs.Entity;
import common.Mailbox;
import common.Message;

import world.components.OxygenComponent;
import world.components.BreakableComponent;
import world.messages.GameRestartMessage;
import world.messages.GameoverMessage;

typedef OxygenEntity = {
    e: Entity,
    oc: OxygenComponent,
    bc: BreakableComponent,
}

class OxygenSystem extends System {

    // 100.00 % == 10000
    public var oxygenLevel: Float;

    public var entities: Map<Int, OxygenEntity>;

    var rand: hxd.Rand;

    public function new() {
        super();
        this.oxygenLevel = 10000;
        this.rand = new Rand(Random.int(0, 1000000));
        this.entities = new Map<Int, OxygenEntity>();
        Mailbox.get("world").listen(GameRestartMessage.TYPE, function(m: Message) {
            this.oxygenLevel = 10000;
        });
    }
    override public function addEntity(entity: Entity) {
        super.addEntity(entity);
        var oc = cast(entity.getComponent(OxygenComponent.TYPE), OxygenComponent);
        if (oc == null) { return; }
        var bc = cast(entity.getComponent(BreakableComponent.TYPE), BreakableComponent);
        this.entities.set(entity.id, { e: entity, oc: oc, bc: bc });
    }
    override public function removeEntity(entity: Entity): Entity {
        this.entities.remove(entity.id);
        return entity;
    }

    override public function update(dt: Float) {
        if (this.world.paused) { return; }
        var flux = 0;
        var leak = 0;
        var consume = 0;
        for (e in this.entities) {
            if (!e.bc.isBroken) { continue; }
            if (e.oc.brokenEffect == Fluctuating) {
                flux += e.oc.amount;
            } else if (e.oc.brokenEffect == Consume) {
                consume += e.oc.amount;
            } else if (e.oc.brokenEffect == Leak) {
                leak += e.oc.amount;
            }
        }
        var fluxBalance = this.rand.random(100) > 50 ? -1 : 1;
        var change: Float = (
                (flux > 0 ? (rand.random(flux) * fluxBalance) : 0)
                - (consume > 0 ? rand.random(consume) : 0)
                + (leak > 0 ? rand.random(leak) : 0)
        );
        if (this.oxygenLevel < 10000) { change += 50; }
        if (this.oxygenLevel > 10200) { change -= 50; }
        this.oxygenLevel += (change * dt);

        if (this.oxygenLevel <= 1000) {
            Mailbox.get("world").dispatch(new GameoverMessage(LowOxygenLevel));
        } else if(this.oxygenLevel >= 15000) {
            Mailbox.get("world").dispatch(new GameoverMessage(HighOxygenLevel));
        }
    }
}
