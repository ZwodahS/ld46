
package world.systems;

import common.ecs.System;
import common.ecs.Entity;
import common.ChunkMap;
import common.Message;
import common.Mailbox;

import world.WorldTile;
import world.components.GridComponent;
import world.components.PlayerComponent;
import world.components.ItemComponent;
import world.components.BreakableComponent;
import world.components.PowerComponent;
import world.messages.MapSetMessage;
import world.messages.RepairEntityMessage;
import world.messages.EntityRepairedMessage;
import world.messages.PickupRequestMessage;
import world.messages.RechargeEnergyMessage;
import world.messages.GameRestartMessage;

class InventorySystem extends common.ecs.System {

    var scene: h2d.Scene;

    var inventoryLayer: h2d.Layers;

    var currentItem: Entity;
    var currentItemComponent: ItemComponent;

    var map: ChunkMap<WorldTile>;

    public function new(scene: h2d.Scene) {
        super();
        this.scene = scene;

        this.inventoryLayer = new h2d.Layers();
        this.inventoryLayer.x = 420;
        this.scene.add(this.inventoryLayer, 1);

        this.setupListeners();
    }

    function setupListeners() {
        var mailbox = Mailbox.get("world");
        mailbox.listen(GameRestartMessage.TYPE, function(m: Message) {
            if (currentItem != null) {
                this.inventoryLayer.removeChild(this.currentItem);
                this.currentItem = null;
                this.currentItemComponent = null;
            }
        });
        mailbox.listen(MapSetMessage.TYPE, function(m: Message) {
            var message = cast(m, MapSetMessage);
            this.map = message.map;
        });

        mailbox.listen(PickupRequestMessage.TYPE, function(m: Message) {
            var message = cast(m, PickupRequestMessage);
            if (message.entity == null) { return; }
            var playerComp = cast(message.entity.getComponent(PlayerComponent.TYPE), PlayerComponent);
            if (playerComp == null) { return; }
            var gridComp = cast(message.entity.getComponent(GridComponent.TYPE), GridComponent);
            if (gridComp == null) { return; }

            var oldItem = this.currentItem;
            this.currentItem = null;
            this.currentItemComponent = null;

            this.pickupItem(gridComp.x, gridComp.y);
            if (oldItem != null) {
                this.dropItem(oldItem, gridComp.x, gridComp.y);
            }
        });

        var mailbox = Mailbox.get("world");
        mailbox.listen(RepairEntityMessage.TYPE, function(m: Message) {
            var message = cast(m, RepairEntityMessage);
            var bc = cast(message.entity.getComponent(BreakableComponent.TYPE), BreakableComponent);
            if (bc == null) { return; }

            if (bc.needItem != null && (this.currentItemComponent == null ||
                    this.currentItemComponent.ability != bc.needItem)) {
                return;
            }
            if (bc.removeWhenRepaired) {
                this.world.removeEntity(message.entity);
            } else {
                bc.isBroken = false;
                mailbox.dispatch(new EntityRepairedMessage(message.entity));
            }
        });

        mailbox.listen(RechargeEnergyMessage.TYPE, function(m: Message) {
            var message = cast(m, RechargeEnergyMessage);
            if (this.currentItemComponent == null || this.currentItemComponent.ability != "cell") {
                return;
            }
            var pc = cast(message.entity.getComponent(PowerComponent.TYPE), PowerComponent);
            if (pc == null) { return; }
            pc.storage = 50;
            this.inventoryLayer.removeChild(this.currentItem);
            this.currentItem = null;
            this.currentItemComponent = null;
        });
    }

    function pickupItem(x: Int, y: Int) {
        var item = this.getItemToPickup(x, y);
        if (item == null) { return; }
        this.currentItem = item.e;
        this.currentItem.x = 0;
        this.currentItem.y = 0;
        this.currentItem.scale(4);
        this.currentItemComponent = item.c;
        this.inventoryLayer.add(this.currentItem, 0);
    }

    function dropItem(item: Entity, x: Int, y: Int) {
        item.scale(0.25);
        this.inventoryLayer.removeChild(item);
        var gridComp = cast(item.getComponent(GridComponent.TYPE), GridComponent);
        gridComp.x = x;
        gridComp.y = y;
        this.world.addEntity(item);
    }

    function getItemToPickup(x: Int, y: Int): {e: Entity, c: ItemComponent } {
        // start from the spot below us
        var worldTile = this.map.get(x, y);
        for (entity in worldTile.entities) {
            var itemComp = cast(entity.getComponent(ItemComponent.TYPE), ItemComponent);
            if (itemComp != null) {
                this.world.removeEntity(entity); // remove the entity from the world;
                return { e: entity, c: itemComp };
            }
        }
        return null;
    }
}
