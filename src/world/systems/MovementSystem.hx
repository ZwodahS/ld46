
package world.systems;

import common.ChunkMap;
import common.Mailbox;
import common.Point2i;
import common.Message;

import common.ecs.System;
import common.ecs.Entity;

import world.WorldTile;
import world.components.GridComponent;
import world.components.SpaceComponent;
import world.components.BreakableComponent;
import world.components.PowerComponent;
import world.messages.RechargeEnergyMessage;
import world.messages.MapSetMessage;
import world.messages.EntityMoveRequestMessage;
import world.messages.EntityCollisionMessage;
import world.messages.EntityMovedMessage;
import world.messages.RepairEntityMessage;
import world.messages.EntityTouchedMessage;

class EntityMovementSystem extends System {

    var map: ChunkMap<WorldTile>;

    public function new() {
        super();

        var mailbox = Mailbox.get("world");
        mailbox.listen(MapSetMessage.TYPE, function(m: Message) {
            var message = cast(m, MapSetMessage);
            this.map = message.map;
        });

        mailbox.listen(EntityMoveRequestMessage.TYPE, function(m: Message) {
            var message = cast(m, EntityMoveRequestMessage);
            this.processMove(message.entity, message.moveAmount);
        });
    }

    function processMove(entity: Entity, move: Point2i) {
        var component = cast(entity.getComponent(GridComponent.TYPE), GridComponent);
        if (component == null) { return; }

        var oldX = component.x;
        var oldY = component.y;
        var newX = component.x + move.x;
        var newY = component.y + move.y;

        var enteringTile = this.map.get(newX, newY);
        if (enteringTile == null) { return; }

        // need to find a few types of entities
        var collidee: Entity = null;

        var spaceComp = cast(entity.getComponent(SpaceComponent.TYPE), SpaceComponent);
        var shouldTestCollision = (spaceComp != null && spaceComp.occupySpace);

        var repairable: Entity = null;
        for (e in enteringTile.entities) {
            var c = cast(e.getComponent(BreakableComponent.TYPE), BreakableComponent);
            if (c != null) {
                if (c.isBroken) {
                    Mailbox.get("world").dispatch(new RepairEntityMessage(e), StartOfQueue);
                    break;
                }
            }
            var pc = cast(e.getComponent(PowerComponent.TYPE), PowerComponent);
            if (pc != null) {
                Mailbox.get("world").dispatch(new RechargeEnergyMessage(e), StartOfQueue);
                break;
            }
        }

        if (shouldTestCollision) {
            for (e in enteringTile.entities) {
                var s = cast(e.getComponent(SpaceComponent.TYPE), SpaceComponent);
                if (s != null && s.occupySpace) {
                    collidee = e;
                    break;
                }
            }
        }

        if (collidee != null) {
            Mailbox.get("world").dispatch(new EntityCollisionMessage(
                entity, collidee,
                [oldX, oldY], [newX, newY]
            ));
            return;
        }

        var exitingTile = this.map.get(component.x, component.y);
        exitingTile.removeEntity(entity);
        enteringTile.addEntity(entity);

        component.x = newX;
        component.y = newY;
        entity.x = Constants.GridSize * component.x;
        entity.y = Constants.GridSize * component.y;

        var touchedEntities = new Array<Entity>();
        for (x in -1...2){
            for (y in -1...2) {
                var tile = this.map.get(newX + x, newY + y);
                if (tile == null) { continue; }
                for (e in tile.entities) {
                    touchedEntities.push(e);
                }
            }
        }
        Mailbox.get("world").dispatch(new EntityTouchedMessage(touchedEntities));
        Mailbox.get("world").dispatch(new EntityMovedMessage(entity, [oldX, oldY], [newX, newY]));
    }
}
