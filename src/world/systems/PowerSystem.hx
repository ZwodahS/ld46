
package world.systems;

import hxd.Rand;

import common.Message;
import common.Mailbox;
import common.ecs.System;
import common.ecs.Entity;

import world.messages.GameoverMessage;
import world.messages.GameRestartMessage;
import world.components.PowerComponent;
import world.components.SurgeComponent;
import world.components.BreakableComponent;

typedef Station = {
    entity: Entity,
    pc: PowerComponent,
}

typedef SurgeEntity = {
    entity: Entity,
    bc: BreakableComponent,
    sc: SurgeComponent,
}

class PowerSystem extends System {

    var stations: Map<Int, Station>;
    var stationsList: Array<Entity>;
    var surgeEntities: Map<Int, SurgeEntity>;

    public var powerLevel(get, null): Float;
    public var powerConsumption(get, null): Float;

    var rand: hxd.Rand;

    public function new() {
        super();
        this.stations = new Map<Int, Station>();
        this.stationsList = new Array<Entity>();
        this.surgeEntities = new Map<Int, SurgeEntity>();
        this.rand = new Rand(Random.int(0, 100000));

        Mailbox.get("world").listen(GameRestartMessage.TYPE, function(m: Message) {
            var message = cast(m, GameRestartMessage);
            for (station in stations) {
                station.pc.storage = 50;
            }
        });
    }

    override public function addEntity(entity: Entity) {
        super.addEntity(entity);
        var pc = cast(entity.getComponent(PowerComponent.TYPE), PowerComponent);
        var sc = cast(entity.getComponent(SurgeComponent.TYPE), SurgeComponent);
        var bc = cast(entity.getComponent(BreakableComponent.TYPE), BreakableComponent);
        if (pc != null) {
            this.stations.set(entity.id, {entity: entity, pc: pc});
            this.stationsList.push(entity);
        }
        if (sc != null && bc != null) {
            this.surgeEntities.set(entity.id, {entity: entity, bc: bc, sc: sc});
        }
    }
    override public function removeEntity(entity: Entity): Entity {
        this.stationsList.remove(entity);
        this.stations.remove(entity.id);
        this.surgeEntities.remove(entity.id);
        return entity;
    }

    public function get_powerLevel(): Float {
        var total: Float = 0;
        for (e in this.stations) {
            total += e.pc.storage;
        }
        return total;
    }

    public function get_powerConsumption(): Float {
        var total: Float = 1.0; // base amount
        for (e in this.surgeEntities) {
            if (!e.bc.isBroken) { continue; }
            total += e.sc.surgeAmount;
        }
        return total;
    }

    override function update(dt: Float) {
        if (this.world.paused) { return; }
        this.stationsList.sort(function(_, _) { return this.rand.random(10) - 5; });
        var consume = this.powerConsumption * dt;
        for (station in this.stationsList) {
            var stationEnt = this.stations.get(station.id);
            if (stationEnt == null) { continue; } // ???
            if (stationEnt.pc.storage >= consume) {
                stationEnt.pc.storage -= consume;
                break;
            } else {
                consume -= stationEnt.pc.storage;
                stationEnt.pc.storage = 0;
            }
        }

        if (this.powerLevel <= 0) {
            Mailbox.get("world").dispatch(new GameoverMessage(NoPower));
        }
    }
}
