
package world.systems;

import common.Point2i;
import common.Mailbox;
import common.ChunkMap;
import common.Message;
import common.ecs.Entity;

import world.Constants;
import world.WorldTile;
import world.components.SpaceComponent;
import world.components.PlayerComponent;
import world.components.GridComponent;
import world.components.RenderComponent;
import world.components.BreakableComponent;
import world.components.LightComponent;
import world.messages.RecalculateFogMessage;
import world.messages.MapSetMessage;
import world.messages.TileChangedMessage;
import world.messages.EntityMovedMessage;
import world.messages.EntityRepairedMessage;
import world.messages.EntitySeenMessage;

typedef InnerEntity = {
    entity: Entity,
    gc: GridComponent,
    lc: LightComponent,
}

class RenderSystem extends common.ecs.System {

    var scene: h2d.Scene;

    var drawLayer: h2d.Layers;
    var tilesLayer: h2d.Layers;
    var entityLayer: h2d.Layers;
    var fogLayer: h2d.Layers;

    var map: ChunkMap<WorldTile>;
    var visibleTiles: List<WorldTile>;

    var lightSources: Map<Int, InnerEntity>;

    public function new(scene: h2d.Scene) {
        super();
        this.scene = scene;
        this.drawLayer = new h2d.Layers();
        this.tilesLayer = new h2d.Layers();
        this.entityLayer = new h2d.Layers();
        this.fogLayer = new h2d.Layers();
        this.visibleTiles = new List<WorldTile>();
        this.lightSources = new Map<Int, InnerEntity>();

        this.drawLayer.add(this.tilesLayer, 5);
        this.drawLayer.add(this.entityLayer, 10);
        this.drawLayer.add(this.fogLayer, 15);
        this.scene.add(this.drawLayer, 0);

        this.setupListeners();

    }

    override public function addEntity(ent: Entity) {
        var rc = cast(ent.getComponent(RenderComponent.TYPE), RenderComponent);
        this.entityLayer.add(ent, rc != null ? rc.index : 0);
        var lc = cast(ent.getComponent(LightComponent.TYPE), LightComponent);
        var gc = cast(ent.getComponent(GridComponent.TYPE), GridComponent);
        if (lc != null && gc != null) {
            this.lightSources.set(ent.id, { entity: ent, lc: lc, gc: gc });
        }
        recalculateFog();
    }

    override public function removeEntity(ent: Entity): Entity {
        this.entityLayer.removeChild(ent);
        this.lightSources.remove(ent.id);
        return ent;
    }

    function setupListeners() {
        var mailbox = Mailbox.get("world");
        mailbox.listen(MapSetMessage.TYPE, function(m: Message) {
            var message = cast(m, MapSetMessage);
            this.map = message.map;
            // init all the fogs
            for (y in 0...Constants.WorldHeight) {
                for (x in 0...Constants.WorldWidth) {
                    var tile = this.map.get(x, y);
                    if (tile != null) {
                        this.tilesLayer.add(tile, 0);
                        // create the fog drawable for the tile
                        var fog = new h2d.Bitmap(
                                h2d.Tile.fromColor(0x000000, Constants.GridSize, Constants.GridSize));
                        fog.x = tile.x;
                        fog.y = tile.y;
                        this.fogLayer.add(fog, 1);
                        fog.alpha = 0.7;
                        tile.fogLayer = fog;
                        this.hideTile(tile);
                    }
                }
            }
            this.recalculateFog();
        });

        mailbox.listen(EntityMovedMessage.TYPE, function(m: Message) {
            var message = cast(m, EntityMovedMessage);
            if (!this.lightSources.exists(message.entity.id)) { return; }
            this.recalculateFog();
        });

        mailbox.listen(RecalculateFogMessage.TYPE, function(m: Message) {
            this.recalculateFog();
        });

        mailbox.listen(EntityRepairedMessage.TYPE, function(m: Message) {
            var message = cast(m, EntityRepairedMessage);
            var entity = message.entity;
            var rc = cast(entity.getComponent(RenderComponent.TYPE), RenderComponent);
            if (rc.state == "broken") {
                rc.state = "default";
            }
        });
    }

    function recalculateFog() {
        if (this.map == null) { return; }
        for (oldTile in visibleTiles) {
            this.hideTile(oldTile);
        }
        visibleTiles.clear();

        for (e in this.lightSources) {
            for (x in -1...2){
                for (y in -1...2) {
                    var position: Point2i = [e.gc.x + x, e.gc.y + y];
                    var tile = this.map.get(position.x, position.y);
                    if (tile == null) { continue; }
                    this.showTile(tile);
                    this.visibleTiles.add(tile);

                    // this checks the position to the left, above, below and right.
                    // if pass, we move in that direction 1 more to show that tile if
                    // this tile is not blocking vision
                    if ((x == 0 && (y == -1 || y == 1)) || (y == 0 && (x == -1 || x == 1))) {
                        if (!this.canBlockVision(tile)) {
                            var affectedTile = this.map.get(position.x + x, position.y + y);
                            if (affectedTile == null) { continue; }
                            this.showTile(affectedTile);
                            this.visibleTiles.add(affectedTile);
                            // go in that direction once more !!
                            if (!this.canBlockVision(affectedTile)) {
                                affectedTile = this.map.get(position.x + x + x, position.y + y + y);
                                if (affectedTile == null) { continue; }
                                this.showTile(affectedTile);
                                this.visibleTiles.add(affectedTile);
                            }
                        }
                    }
                    // diagonal
                    if ((x == -1 || x == 1) && (y == -1 || y == 1)) {
                        if (!this.canBlockVision(tile)){
                            var affectedTile1 = this.map.get(position.x + x, position.y);
                            var affectedTile2 = this.map.get(position.x, position.y + y);
                            if (affectedTile1 != null) {
                                this.showTile(affectedTile1);
                                this.visibleTiles.add(affectedTile1);
                            }
                            if (affectedTile2 != null ) {
                                this.showTile(affectedTile2);
                                this.visibleTiles.add(affectedTile2);
                            }
                        }
                    }
                }
            }
        }

        var entitiesSeen = new Array<Entity>();
        for (tile in this.visibleTiles) {
            for (e in tile.entities) {
                entitiesSeen.push(e);
            }
        }
        Mailbox.get("world").dispatch(new EntitySeenMessage(entitiesSeen));
    }

    function showTile(tile: WorldTile) {
        tile.fog = false;
        for (entity in tile.entities) {
            var rc = cast(entity.getComponent(RenderComponent.TYPE), RenderComponent);
            if (rc == null) { continue; }
            var bc = cast(entity.getComponent(BreakableComponent.TYPE), BreakableComponent);
            if (bc != null && bc.isBroken) {
                rc.state = "broken";
            } else {
                rc.state = "default";
            }
        }
    }

    function hideTile(tile: WorldTile) {
        tile.fog = true;
        for (entity in tile.entities) {
            var rc = cast(entity.getComponent(RenderComponent.TYPE), RenderComponent);
            if (rc == null) { continue; }
            rc.state = "fog";
        }
    }

    function canBlockVision(tile: WorldTile): Bool {
        for (e in tile.entities) {
            var s = cast(e.getComponent(SpaceComponent.TYPE), SpaceComponent);
            if (s == null) { continue; }
            if (s.blockVision) { return true; }
        }
        return false;
    }
}
