
package world.systems;

import common.Mailbox;
import common.Message;
import common.ecs.System;
import common.ecs.Entity;

import world.messages.GameRestartMessage;
import world.components.PlayerComponent;
import world.components.GridComponent;
import world.messages.EntityMoveRequestMessage;
import world.messages.PickupRequestMessage;

class InputSystem extends System {

    var player: Entity;

    public function new() {
        super();

        var mailbox = Mailbox.get("world");
        mailbox.listen(GameRestartMessage.TYPE, function(m: Message) {
            var p = this.player;
            this.world.removeEntity(this.player);
            var gc = cast(p.getComponent(GridComponent.TYPE), GridComponent);
            gc.x = 15;
            gc.y = 8;
            this.world.addEntity(p);
        });
    }

    override public function onEvent(event: hxd.Event): Bool {
        if (event.kind == hxd.Event.EventKind.EKeyDown) {
            switch(event.keyCode) {
                case hxd.Key.A:
                    this.processMove(-1, 0);
                case hxd.Key.H:
                    this.processMove(-1, 0);
                case hxd.Key.W:
                    this.processMove(0, -1);
                case hxd.Key.K:
                    this.processMove(0, -1);
                case hxd.Key.D:
                    this.processMove(1, 0);
                case hxd.Key.L:
                    this.processMove(1, 0);
                case hxd.Key.S:
                    this.processMove(0, 1);
                case hxd.Key.J:
                    this.processMove(0, 1);
                case hxd.Key.SPACE:
                    this.processInventory();
                default:
            }
        }
        return false;
    }

    override public function addEntity(ent: Entity) {
        if (ent.getComponent(PlayerComponent.TYPE) != null) {
            this.player = ent;
        }
    }
    override public function removeEntity(ent: Entity): Entity {
        if (this.player == ent) {
            this.player = null;
        }
        return ent;
    }

    function processMove(x: Int, y: Int) {
        if (this.world.paused) { return; }
        if (this.player != null) {
            Mailbox.get("world").dispatch(
                new EntityMoveRequestMessage(this.player, [x, y]),
                DispatchMode.StartOfQueue
            );
        }
    }

    function processInventory() {
        if (this.world.paused) { return; }
        Mailbox.get("world").dispatch(new PickupRequestMessage(this.player));
    }

}
