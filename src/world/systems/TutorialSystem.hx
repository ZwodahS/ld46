
package world.systems;

import common.Mailbox;
import common.Message;
import common.ecs.System;
import common.ecs.Entity;

import world.components.GridComponent;
import world.components.TutorialComponent;
import world.components.BreakableComponent;
import world.components.PowerComponent;
import world.components.ItemComponent;

import world.messages.GameRestartMessage;
import world.messages.GameoverMessage;
import world.messages.ShowChatMessage;
import world.messages.ShowInfoMessage;
import world.messages.HideChatMessage;
import world.messages.EntitySeenMessage;
import world.messages.EntityTouchedMessage;
import world.messages.IncidentRateIncreaseMessage;

enum TutorialState {
    Idle;
    Beginning;
    InfoTooltip;
    GameOver;
    Victory;
}

class TutorialSystem extends System {

    var state: TutorialState;

    var tutorial: Bool = true;
    var shownBrokenVent = false;
    var shownBrokenTank = false;
    var shownBrokenWire = false;
    var shownPowerStation = false;
    var shownPowerStationLow = false;
    var shownEnergyCell = false;

    var shownVictory = false;
    var timePassed: Float;

    var entities: List<Entity>;

    public function new() {
        super();
        this.setupListeners();
        this.state = Idle;
        this.entities = new List<Entity>();
    }

    override public function addEntity(entity: Entity) {
        this.entities.add(entity);
    }

    override public function removeEntity(entity: Entity): Entity {
        this.entities.remove(entity);
        return entity;
    }

    function setupListeners() {
        var mailbox = Mailbox.get("world");
        mailbox.listen(GameRestartMessage.TYPE, function(m: Message) {
            var toRemove = new List<Entity>();
            for (entity in entities) {
                var tc = cast(entity.getComponent(TutorialComponent.TYPE), TutorialComponent);
                if (tc == null) {
                    continue;
                }
                if (tc.name == "fire") {
                    toRemove.push(entity);
                    continue;
                }
                var bc = cast(entity.getComponent(BreakableComponent.TYPE), BreakableComponent);
                if (bc != null) {
                    bc.isBroken = false;
                }
                var ic = cast(entity.getComponent(ItemComponent.TYPE), ItemComponent);
                if (ic != null) {
                    this.world.removeEntity(entity);
                }
            }
            for (e in toRemove){
                this.world.removeEntity(e);
            }
            for (position in [ [14, 9], [15, 9] ]) {
                var fire = Factory.get().makeFire(position[0], position[1]);
                this.world.addEntity(fire);
            }
            var e = Factory.get().makeExtinguisher(18, 8);
            this.world.addEntity(e);

            for (x in 1...6) {
                for (y in 1...4) {
                    e = Factory.get().makeCell(x, y);
                    this.world.addEntity(e);
                }
            }
            mailbox.dispatch(new ShowChatMessage(Constants.FirstMessage));
            this.state = Beginning;
            this.world.paused = true;
            this.shownVictory = false;
            this.timePassed = 0;
        });

        mailbox.listen(GameoverMessage.TYPE, function(m: Message) {
            var message = cast(m, GameoverMessage);
            switch(message.reason) {
                case LowOxygenLevel:
                    this.gameover(Constants.LoseLowOxygen);
                case HighOxygenLevel:
                    this.gameover(Constants.LoseHighOxygen);
                case NoPower:
                    this.gameover(Constants.LoseNoPower);
                default:
            }
        });

        mailbox.listen(EntitySeenMessage.TYPE, function(m: Message) {
            var message = cast(m, EntitySeenMessage);
            if (!this.tutorial) { return; }
            for (entity in message.entities) {
                var tc = cast(entity.getComponent(TutorialComponent.TYPE), TutorialComponent);
                if (tc == null) { continue; }
                switch(tc.name) {
                    case "vent":
                        if (this.shownBrokenVent) { continue; }
                        if (this.isBroken(entity)) {
                            show(entity, Constants.BrokenVentMessage);
                            this.shownBrokenVent = true;
                            break;
                        }
                    case "tank":
                        if (this.shownBrokenTank) { continue; }
                        if (this.isBroken(entity)) {
                            show(entity, Constants.BrokenTankMessage);
                            this.shownBrokenTank = true;
                            break;
                        }
                    case "wire":
                        if (this.shownBrokenWire) { continue; }
                        if (this.isBroken(entity)) {
                            show(entity, Constants.BrokenWireMessage);
                            this.shownBrokenWire = true;
                            break;
                        }
                    case "station":
                        if (this.shownPowerStation) { continue; }
                        show(entity, Constants.PowerStationMessage);
                        this.shownPowerStation = true;
                        break;
                    case "cell":
                        if (this.shownEnergyCell) { continue; }
                        show(entity, Constants.EnergyCell);
                        this.shownEnergyCell = true;
                        break;
                    default:
                }
            }
        });

        mailbox.listen(EntityTouchedMessage.TYPE, function(m: Message) {
            if (this.shownPowerStationLow) { return; }
            var message = cast(m, EntityTouchedMessage);
            for (e in message.entities) {
                var tc = cast(e.getComponent(TutorialComponent.TYPE), TutorialComponent);
                if (tc == null) { continue; }
                if (tc.name != "station") { continue; }
                var pc = cast(e.getComponent(PowerComponent.TYPE), PowerComponent);
                if (pc == null) { continue; }
                if (pc.storage < 15) {
                    show(e, Constants.PowerStationLowMessage);
                    this.shownPowerStationLow = true;
                }
            }
        });
    }

    function isBroken(entity: Entity) {
        var bc = cast(entity.getComponent(BreakableComponent.TYPE), BreakableComponent);
        if (bc == null) { return false; }
        return bc.isBroken;
    }

    function show(entity: Entity, text: String) {
        var gc = cast(entity.getComponent(GridComponent.TYPE), GridComponent);
        if (gc == null) { return; }
        var mailbox = Mailbox.get("world");
        mailbox.dispatch(new ShowInfoMessage([gc.x, gc.y], "mark"));
        mailbox.dispatch(new ShowChatMessage(text, gc.y < 10 ? "bottom" : "top"));
        this.state = InfoTooltip;
        this.world.paused = true;
    }

    override public function onEvent(event: hxd.Event): Bool {
        if (this.state == Idle) { return false; }
        if (event.kind == hxd.Event.EventKind.EKeyDown) {
            switch(event.keyCode) {
                case hxd.Key.R:
                    this.closeChat();
                    Mailbox.get("world").dispatch(new GameRestartMessage(), EndOfQueue);
                case hxd.Key.SPACE:
                    this.closeChat();
                default:
            }
        }
        return false;
    }

    function gameover(text: String) {
        Mailbox.get("world").dispatch(new ShowChatMessage(text));
        this.state = GameOver;
        this.world.paused = true;
    }

    override public function update(dt: Float) {
        if (this.world.paused) { return; }
        this.timePassed += dt;
        if (!this.shownVictory && timePassed > Constants.VictoryTimer){
            Mailbox.get("world").dispatch(new ShowChatMessage(Constants.VictoryMessage));
            this.shownVictory = true;
            this.state = Victory;
            this.world.paused = true;
            Mailbox.get("world").dispatch(new IncidentRateIncreaseMessage(1.0));
            return;
        }

    }

    public function closeChat() {
        if (this.state == InfoTooltip) {
            Mailbox.get("world").dispatch(new ShowInfoMessage(null, null));
        }

        if (this.state == Beginning || this.state == InfoTooltip
                || this.state == Victory || this.state == GameOver) {
            Mailbox.get("world").dispatch(new HideChatMessage());
            this.state = Idle;
            this.world.paused = false;
        }
    }
}
