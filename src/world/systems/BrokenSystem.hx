package world.systems;

import common.Mailbox;
import common.Message;
import common.ecs.Component;
import common.ecs.System;
import common.ecs.Entity;

import world.components.BreakableComponent;

import world.messages.RecalculateFogMessage;
import world.messages.EntityBrokeMessage;
import world.messages.BreakEntityMessage;

class BrokenSystem extends System {
    public function new() {
        super();
        this.setupListeners();
    }
    override public function addEntity(entity: Entity) {}
    override public function removeEntity(entity: Entity): Entity {
        return entity;
    }

    function setupListeners() {
        var mailbox = Mailbox.get("world");
        mailbox.listen(BreakEntityMessage.TYPE, function(m: Message) {
            var message = cast(m, BreakEntityMessage);
            var bc = cast(message.entity.getComponent(BreakableComponent.TYPE), BreakableComponent);
            if (bc == null) { return; }
            if (bc.isBroken) { return; } // already broken
            bc.isBroken = true;
            mailbox.dispatch(new EntityBrokeMessage(message.entity));
            mailbox.dispatch(new RecalculateFogMessage());
        });
    }

}
