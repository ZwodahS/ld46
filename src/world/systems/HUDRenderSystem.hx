
package world.systems;

import common.Mailbox;
import common.Assets;
import common.Point2i;
import common.Message;
import common.ecs.Entity;

import world.World;
import world.components.ControlComponent;
import world.components.PowerComponent;
import world.messages.ShowInfoMessage;
import world.messages.EntityTouchedMessage;
import world.messages.ShowChatMessage;
import world.messages.HideChatMessage;

class HUDRenderSystem extends common.ecs.System {

    var scene: h2d.Scene;

    var statsLayer: h2d.Layers;
    var powerText: h2d.Text;
    var oxygenLevelText: h2d.Text;

    var infoLayer: h2d.Layers;
    var info: h2d.Object = null;
    var border: h2d.Object = null;

    var chatLayer: h2d.Layers;
    var chatText: h2d.Text;


    var w: World;

    var assets: Assets;

    var controlEntities: Map<Int, Entity>;

    var showPowerComponent: PowerComponent = null;
    var showControlComponent: ControlComponent = null;

    public function new(assets: Assets, scene: h2d.Scene) {
        super();
        this.scene = scene;
        this.assets = assets;
        this.statsLayer = new h2d.Layers();
        this.infoLayer = new h2d.Layers();
        this.controlEntities = new Map<Int, Entity>();

        var fontH1 = hxd.res.DefaultFont.get().clone();
        fontH1.resizeTo(12);

        this.powerText = common.Factory.createH2dText(
                fontH1, "", [20, 2], h3d.Vector.fromColor(0xFF000000));
        this.oxygenLevelText = common.Factory.createH2dText(
                fontH1, "", [110, 2], h3d.Vector.fromColor(0xFF000000));

        var hud = assets.getAsset("hud").getBitmap(0);
        this.statsLayer.add(hud, 0);
        this.statsLayer.add(this.powerText, 2);
        this.statsLayer.add(this.oxygenLevelText, 2);
        this.statsLayer.x = 160;
        this.statsLayer.visible = false;

        this.border = assets.getAsset("info_border").getBitmap(0);
        this.border.visible = false;
        this.infoLayer.add(border, 0);
        this.scene.add(this.infoLayer, 9);
        this.scene.add(this.statsLayer, 10);

        this.chatLayer = new h2d.Layers();
        var textFont = hxd.res.DefaultFont.get().clone();
        textFont.resizeTo(6);
        this.chatText = common.Factory.createH2dText(
                textFont, "", h3d.Vector.fromColor(0xFFFFFFFF));
        var p = assets.getAsset("portrait").getBitmap();
        p.x = -1;
        p.y = -1;
        this.chatLayer.add(p, 0);
        p = assets.getAsset("textbox").getBitmap();
        p.y = -1;
        p.x = 96;
        this.chatLayer.add(p, 0);
        chatText.x = 100;
        chatText.y = 10;
        chatText.maxWidth = 300;

        for (y in 0...7) {
            for (x in 0...5) {
                var anim = this.assets.getAsset("static").getAnim(4,
                        function(_, _) { return Random.int(0, 10) - 5; });
                anim.x = x * Constants.GridSize;
                anim.y = y * Constants.GridSize;
                anim.alpha = 0.5;
                this.chatLayer.add(anim, 2);
            }
        }
        this.chatLayer.add(this.chatText, 1);
        this.chatLayer.x = 32;
        this.chatLayer.y = 32;
        this.chatLayer.visible = false;
        this.scene.add(this.chatLayer, 11);

        this.setupListeners();
    }

    override public function init(world: common.ecs.World) {
        super.init(world);
        var w: World = cast(world, World);
        if (w != null) {
            this.w = w;
        }
    }

    override public function addEntity(entity: Entity) {
        super.addEntity(entity);
        var cc = cast(entity.getComponent(ControlComponent.TYPE), ControlComponent);
        if (cc != null) {
            this.controlEntities[entity.id] = (entity);
        }
    }

    function setupListeners() {
        var mailbox = Mailbox.get("world");
        mailbox.listen(ShowInfoMessage.TYPE, function(m: Message) {
            var message = cast(m, ShowInfoMessage);
            this.showInfo(message.position, message.infoType);
        });

        mailbox.listen(EntityTouchedMessage.TYPE, function(m: Message) {
            var message = cast(m, EntityTouchedMessage);
            var hasControl = false;
            this.showPowerComponent = null;
            this.showControlComponent = null;
            for (e in message.entities) {
                if (this.controlEntities.exists(e.id)) {
                    this.showPowerComponent = cast(e.getComponent(PowerComponent.TYPE), PowerComponent);
                    this.showControlComponent = cast(e.getComponent(ControlComponent.TYPE), ControlComponent);
                    hasControl = true;
                    break;
                }
            }
            this.statsLayer.visible = hasControl;
            this.oxygenLevelText.visible = (this.showControlComponent != null && this.showControlComponent.showAllOxygen);
            this.powerText.visible = (this.showControlComponent != null && (
                        this.showControlComponent.showAllPower || this.showControlComponent.showThisPower));
        });

        mailbox.listen(ShowChatMessage.TYPE, function(m: Message) {
            var message = cast(m, ShowChatMessage);
            this.showChat(message.text, message.location);
        });

        mailbox.listen(HideChatMessage.TYPE, function(m: Message) {
            this.hideChat();
        });
    }

    function showInfo(position: Point2i, type: String) {
        this.hideInfo();
        if (type == "space") {
            this.info = this.assets.getAsset("info_space").getBitmap(position.x > 30 ? 1 : 0);
            var offsetX = position.x > 30 ? -48 : 16;
            this.info.x = offsetX + (position.x * Constants.GridSize);
            this.info.y = position.y * Constants.GridSize;
            this.infoLayer.add(this.info, 0);
            this.border.visible = true;
            this.border.x = position.x * Constants.GridSize;
            this.border.y = position.y * Constants.GridSize;
        } else if (type == "mark") {
            this.info = this.assets.getAsset("info_mark").getBitmap(position.x > 30 ? 1 : 0);
            var offsetX = position.x > 30 ? -48 : 16;
            this.info.x = offsetX + (position.x * Constants.GridSize);
            this.info.y = position.y * Constants.GridSize;
            this.infoLayer.add(this.info, 0);
            this.border.visible = true;
            this.border.x = position.x * Constants.GridSize;
            this.border.y = position.y * Constants.GridSize;
        } else if (type == null) {}
    }

    function hideInfo() {
        if (this.info != null) {
            this.infoLayer.removeChild(this.info);
            this.info = null;
            this.border.visible = false;
        }
    }

    override public function update(dt: Float) {
        if (this.world.paused) { return; }
        if (this.w != null) {
            if (this.showControlComponent != null) {
                if(this.showControlComponent.showAllPower) {
                    this.powerText.text = '${Std.int(this.w.power)} ( -${this.w.powerConsumption}/s)';
                } else if (this.showControlComponent.showThisPower) {
                    this.powerText.text = '${Std.int(this.showPowerComponent.storage)}';
                }
                if (this.showControlComponent.showAllOxygen) {
                    this.oxygenLevelText.text = '${this.w.oxygenLevel} %';
                }
            }

        }
    }

    function showChat(text: String, location: String) {
        this.chatText.text = text;
        this.chatLayer.visible = true;
        if (location == "top") {
            this.chatLayer.y = 6;
        } else {
            this.chatLayer.y = 160;
        }
    }

    function hideChat() {
        this.chatLayer.visible = false;
    }
}
