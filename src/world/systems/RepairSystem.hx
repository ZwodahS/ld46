
package world.systems;

import common.Mailbox;
import common.Message;
import common.ecs.System;
import common.ecs.Entity;

import world.messages.RepairEntityMessage;
import world.messages.EntityRepairedMessage;
import world.components.BreakableComponent;

class RepairSystem extends System {
    public function new() {
        super();
        // DEPRECATED, moved to InventorySystem, lazy to pass the current item around

    }
}
