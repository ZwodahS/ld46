
package world.components;
import common.ecs.Component;

class PowerComponent extends Component {
    public static final TYPE = "PowerComponent";
    override public function get_type(): String { return TYPE; }

    public var storage: Float;

    public function new(amount: Float = 50) {
        super();
        this.storage = amount;
    }
}
