
package world.components;

import common.ecs.Component;

class GridComponent extends Component {
    public static final TYPE = "GridComponent";
    override public function get_type(): String { return TYPE; }

    public var x: Int;
    public var y: Int;

    public function new(x: Int=0, y: Int=0) {
        super();
        this.x = x;
        this.y = y;
    }
}
