
package world.components;

import common.ecs.Component;

class ItemComponent extends Component {
    public static final TYPE = "ItemComponent";
    override public function get_type(): String { return TYPE; }

    public var ability(default, null): String;
    public var consumedWhenUsed: Bool;

    // hardcode the ability to a string
    public function new(ability: String, consumedWhenUsed = false) {
        super();
        this.ability = ability;
        this.consumedWhenUsed = consumedWhenUsed;
    }
}
