
package world.components;

import common.ecs.Component;

class LightComponent extends Component {
    public static final TYPE = "LightComponent";
    override public function get_type(): String { return TYPE; }
}
