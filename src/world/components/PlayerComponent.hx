
package world.components;

import common.ecs.Component;

class PlayerComponent extends Component {
    public static final TYPE = "PlayerComponent";
    override public function get_type(): String { return TYPE; }
}
