
package world.components;
import common.ecs.Component;

class SurgeComponent extends Component {
    public static final TYPE = "SurgeComponent";
    override public function get_type(): String { return TYPE; }

    public var surgeAmount: Float = 0.25;
}
