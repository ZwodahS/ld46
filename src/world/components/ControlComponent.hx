
package world.components;
import common.ecs.Component;

class ControlComponent extends Component {
    public static final TYPE = "ControlComponent";
    override public function get_type(): String { return TYPE; }

    public var showAllPower = true;
    public var showAllOxygen = true;
    public var showThisPower = false;
}
