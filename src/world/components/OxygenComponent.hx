
package world.components;
import common.ecs.Component;

enum Effect {
    Fluctuating;
    Leak;
    Consume;
}

class OxygenComponent extends Component {
    public static final TYPE = "OxygenComponent";
    override public function get_type(): String { return TYPE; }

    public var brokenEffect: Effect;
    public var amount: Int;
    public function new(brokenEffect: Effect, amount: Int) {
        super();
        this.brokenEffect = brokenEffect;
        this.amount = amount;
    }
}
