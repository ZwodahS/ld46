
package world.components;

import common.ecs.Component;

class BreakableComponent extends Component {
    public static final TYPE = "BreakableComponent";
    override public function get_type(): String { return TYPE; }

    public var isBroken = false;
    public var removeWhenRepaired = false;
    public var needItem: String = null;

    public function new(isBroken: Bool = false, removeWhenRepaired: Bool = false, needItem: String = null) {
        super();
        this.isBroken = isBroken;
        this.removeWhenRepaired = removeWhenRepaired;
        this.needItem = needItem;
    }
}
