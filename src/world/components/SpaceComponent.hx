
package world.components;

import common.ecs.Component;

class SpaceComponent extends Component {
    public static final TYPE = "SpaceComponent";
    override public function get_type(): String { return TYPE; }

    public var occupySpace: Bool;
    public var blockVision: Bool;

    public function new(occupySpace: Bool = true, blockVision: Bool = false) {
        super();
        this.occupySpace = occupySpace;
        this.blockVision = blockVision;
    }
}
