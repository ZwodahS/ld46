
package world.components;

import common.ecs.Component;
import common.ecs.Entity;

class RenderComponent extends Component {
    public static final TYPE = "RenderComponent";
    override public function get_type(): String { return TYPE; }

    public var index: Int = 0;
    public var draws: Map<String, h2d.Object>;

    public var state(default, set): String;

    public function new(draws: Map<String, h2d.Object>, initialState: String = "default") {
        super();
        this.draws = draws;
        this.state = initialState;
    }

    override public function set_entity(e: Entity): Entity {
        super.set_entity(e);
        for (obj in this.draws) {
            if (obj != null) {
                e.add(obj, 0);
            }
        }
        return this.entity;
    }

    public function set_state(state: String): String {
        this.state = state;
        // this found allow us to use the same h2d.Object for different key.
        // if this is not here, we will set up setting it visible, and setting it invisible again
        var found: h2d.Object = null;
        for (k => obj in this.draws) {
            if (k != state && obj != found) {
                obj.visible = false;
            } else {
                obj.visible = true;
                found = obj;
            }
        }
        return this.state;
    }
}
