
package world.components;
import common.ecs.Component;

class TutorialComponent extends Component {
    public static final TYPE = "TutorialComponent";
    override public function get_type(): String { return TYPE; }

    public var name: String;
    public function new(name: String) {
        super();
        this.name = name;
    }
}

